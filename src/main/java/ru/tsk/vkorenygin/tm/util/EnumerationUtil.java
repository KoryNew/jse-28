package ru.tsk.vkorenygin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.enumerated.Sort;
import ru.tsk.vkorenygin.tm.enumerated.Status;

import java.util.Arrays;

public interface EnumerationUtil {

    static @Nullable Status parseStatus(@NotNull final String name) {
        return Arrays
                .stream(Status.values())
                .filter(status -> status.name().equals(name))
                .findFirst().orElse(null);
    }

    static @Nullable Sort parseSort(@NotNull final String name) {
        return Arrays
                .stream(Sort.values())
                .filter(sort -> sort.name().equals(name))
                .findFirst().orElse(null);
    }

    static @Nullable Role parseRole(@NotNull final String name) {
        return Arrays
                .stream(Role.values())
                .filter(role -> role.name().equals(name))
                .findFirst().orElse(null);
    }

}
