package ru.tsk.vkorenygin.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    static @NotNull String nextLine() {
        return SCANNER.nextLine();
    }

    static int nextNumber() {
        @NotNull final String value = SCANNER.nextLine();
        try {
            return Integer.parseInt(value);
        } catch (RuntimeException e) {
            throw new IncorrectIndexException(value);
        }
    }

}
