package ru.tsk.vkorenygin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractDataCommand;
import ru.tsk.vkorenygin.tm.dto.Domain;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataLoadBase64Command extends AbstractDataCommand {

    @Nullable
    @Override
    public  String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-base64";
    }

    @NotNull
    @Override
    public String description() {
        return "Load BASE64 data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD BASE64 DATA]");
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
