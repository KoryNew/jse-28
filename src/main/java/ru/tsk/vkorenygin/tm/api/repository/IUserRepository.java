package ru.tsk.vkorenygin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsk.vkorenygin.tm.entity.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    Optional<User> findByLogin(@NotNull final String login);

    @NotNull
    Optional<User> findByEmail(@NotNull final String email);

    @NotNull
    Optional<User> removeByLogin(@NotNull final String login);

}
