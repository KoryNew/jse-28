package ru.tsk.vkorenygin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.entity.Project;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

import java.util.Optional;

public interface IProjectService extends IOwnerService<Project> {

    void create(@Nullable final String name, @Nullable final String userId) throws AbstractException;

    void create(@Nullable final String name,
                @Nullable final String description,
                @Nullable final String userId) throws AbstractException;

    @NotNull
    Optional<Project> findByName(@Nullable final String name, @Nullable final String userId) throws AbstractException;

    @NotNull
    Project changeStatusById(@Nullable final String id,
                             @Nullable final Status status,
                             @Nullable final String userId) throws AbstractException;

    @NotNull
    Project changeStatusByName(@Nullable final String name,
                               @Nullable final Status status,
                               @Nullable final String userId) throws AbstractException;

    @NotNull
    Project changeStatusByIndex(@Nullable final Integer index,
                                @Nullable final Status status,
                                @Nullable final String userId) throws AbstractException;

    @NotNull
    Project startById(@Nullable final String id, @Nullable final String userId) throws AbstractException;

    @NotNull
    Project startByIndex(@Nullable final Integer index, @Nullable final String userId) throws AbstractException;

    @NotNull
    Project startByName(@Nullable final String name, @Nullable final String userId) throws AbstractException;

    @NotNull
    Project finishById(@Nullable final String id, @Nullable final String userId) throws AbstractException;

    @NotNull
    Project finishByIndex(@Nullable final Integer index, @Nullable final String userId) throws AbstractException;

    @NotNull
    Project finishByName(@Nullable final String name, @Nullable final String userId) throws AbstractException;

    @NotNull
    Project updateById(@Nullable final String id,
                       @Nullable final String name,
                       @Nullable final String description,
                       @Nullable final String userId) throws AbstractException;

    @NotNull
    Project updateByIndex(@Nullable final Integer index,
                          @Nullable final String name,
                          @Nullable final String description,
                          @Nullable final String userId) throws AbstractException;

    @NotNull
    Optional<Project> removeByName(@Nullable final String name, @Nullable final String userId) throws AbstractException;

}
