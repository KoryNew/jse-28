package ru.tsk.vkorenygin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.entity.User;
import ru.tsk.vkorenygin.tm.enumerated.Role;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;

public interface IAuthService {

    @NotNull
    User getUser() throws AbstractException;

    @NotNull
    String getUserId() throws AccessDeniedException;

    boolean isAuth();

    void logout();

    void login(@Nullable final String login, @Nullable final String password) throws AbstractException;

    void registry(@Nullable final String login,
                  @Nullable final String password,
                  @Nullable final String email) throws AbstractException;

    void checkRoles(@Nullable Role... roles) throws AbstractException;

}
