package ru.tsk.vkorenygin.tm;


import ru.tsk.vkorenygin.tm.bootstrap.Bootstrap;
import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class Application {

    public static void main(String[] args) throws AbstractException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}